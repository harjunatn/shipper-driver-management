import classNames from "classnames";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

export default function Search(props) {
  const { children, className, ...restProps } = props;
  const searchClass = classNames("border-2 rounded-md p-3 pl-8", className);

  return (
    <>
      <input type="text" {...restProps} className={searchClass}>
        {children}
      </input>
      <FontAwesomeIcon
        icon={faSearch}
        className="absolute left-2 top-4 text-lg text-orange-950"
      />
    </>
  );
}
