import Image from "next/image";
import classNames from "classnames";

import UserPlaceholser from "/assets/user-placeholder.png";
import { formatDate } from "../../utils/date";

export default function Card(props) {
  const { user, className, ...restProps } = props;
  const { name, cell, email, registered, location } = user;
  const cardClass = classNames("bg-white", className);

  return (
    <div className={cardClass} {...restProps} data-testid="card-container">
      <div className="sm:w-72">
        <div className="flex justify-between border-b-2 px-5 py-3">
          <p className="text-gray-300">
            Driver ID{" "}
            <span className="text-orange-950">{location.postcode}</span>
          </p>
          <p className="text-gray-300 text-3xl leading-3">...</p>
        </div>
        <div className="px-5 py-10">
          <Image
            src={UserPlaceholser}
            alt="user-placeholder"
            width={100}
            height={100}
            className="rounded-full"
          />
          <div className="mt-5">
            <div className="mb-2">
              <p className="text-gray-500 text-xs p-0">Nama Driver</p>
              <p>
                {name.first}, {name.last}
              </p>
            </div>
            <div className="mb-2">
              <p className="text-gray-500 text-xs p-0">Telepon</p>
              <p>{cell}</p>
            </div>
            <div className="mb-2">
              <p className="text-gray-500 text-xs p-0">Email</p>
              <p>{email}</p>
            </div>
            <div className="mb-2">
              <p className="text-gray-500 text-xs p-0">Tanggal Lahir</p>
              <p>{formatDate(registered.date)}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
