/** @type {import('next').NextConfig} */
const isProd = process.env.NODE_ENV === "production";

const nextConfig = {
  reactStrictMode: true,

  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },

  images: {
    loader: "imgix",
    path: "https://gitlab.com/harjunatn/shipper-driver-management/",
  },

  assetPrefix: isProd ? "/shipper-driver-management/" : "",
};

module.exports = nextConfig;
