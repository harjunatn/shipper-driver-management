import { useState, useMemo, useEffect } from "react";
import classNames from "classnames";

import { Card, Search } from "../components";

export async function getStaticProps() {
  try {
    const res = await fetch("https://randomuser.me/api/?results=30");
    const users = await res.json();

    if (!users) {
      return { notFound: true };
    }

    return { props: { users: users.results } };
  } catch (error) {
    return { notFound: true };
  }
}

export default function Driver({ users }) {
  const [user, setUser] = useState([]);
  const [filter, setFilter] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const limitPerPage = 5;

  const filteredDriver = useMemo(
    () =>
      user &&
      user.filter((val) =>
        val.name.first.toLowerCase().includes(filter.toLowerCase())
      ),
    [filter, user]
  );

  const indexLastDriver = currentPage * limitPerPage;
  const indexFristDriver = indexLastDriver - limitPerPage;
  const currentIndex = filteredDriver?.slice(indexFristDriver, indexLastDriver);
  const maxLimitPage =
    filteredDriver && Math.ceil(filteredDriver.length / limitPerPage);

  function onChangeSearch(e) {
    setFilter(e.target.value);
    setCurrentPage(1);
  }

  useEffect(() => {
    const userCache = JSON.parse(localStorage.getItem("users"));

    if (userCache) {
      setUser(userCache);
    } else {
      localStorage.setItem("users", JSON.stringify(users));
      setUser(users);
    }
  }, []);

  return (
    <div>
      <div
        className="sm:flex justify-between items-center bg-white p-5 mb-5"
        data-testid="filter-container"
      >
        <div>
          <p className="font-bold text-xl sm:text-3xl text-orange-950">
            DRIVER MANAGEMENT
          </p>
          <p className="mb-5 text-sm sm:text-base text-gray-500">
            Data driver yang bekerja dengan Anda.
          </p>
        </div>
        <div className="relative">
          <Search
            type="text"
            value={filter}
            onChange={onChangeSearch}
            placeholder="Cari driver"
            className="w-full sm:w-auto sm:mr-5 mb-5 sm:mb-0"
            data-testid="filter-input"
          />
          <button
            className="text-left sm:text-center w-full sm:w-auto bg-orange-950 text-white p-3"
            data-testid="filter-button"
          >
            TAMBAH DRIVER +
          </button>
        </div>
      </div>

      <div className="flex flex-wrap sm:flex-nowrap sm:overflow-x-auto sm:-mr-10 pb-5">
        {filteredDriver?.length === 0 ? (
          <p>Driver tidak ditemukan</p>
        ) : (
          currentIndex?.map((user, index) => (
            <Card
              user={user}
              key={index}
              className="w-full mb-5 sm:mb-0 sm:mr-5"
            />
          ))
        )}
      </div>
      <div className="text-center sm:mt-10 mb-10">
        <button
          disabled={currentPage === 1}
          onClick={() => setCurrentPage(currentPage - 1)}
          className={classNames(
            "mr-10",
            currentPage === 1 && "cursor-not-allowed text-gray-300"
          )}
        >
          {"<"} Previous Page
        </button>
        <button
          disabled={currentPage === maxLimitPage}
          onClick={() => setCurrentPage(currentPage + 1)}
          className={classNames(
            "ml-10",
            currentPage === maxLimitPage && "cursor-not-allowed text-gray-300"
          )}
        >
          Next Page {">"}
        </button>
      </div>
    </div>
  );
}
