import Image from "next/image";
import ShipperLogo from "/assets/shipper-logo.png";
import UserPlaceholder from "/assets/user-placeholder.png";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

export default function Topbar({ onclickShowMenu }) {
  return (
    <div className="flex justify-between items-center px-5 bg-white">
      <div className="flex items-center">
        <div className="block sm:hidden" onClick={() => onclickShowMenu()}>
          <FontAwesomeIcon icon={faBars} className="text-2xl mr-5" />
        </div>

        <Image
          src={ShipperLogo}
          alt="shipper-logo"
          width={120}
          height={80}
          className="object-contain"
        />
      </div>
      <div className="flex items-center">
        <p className="hidden sm:block p-0 mr-5">
          Hello, <span className="text-orange-950">Shipper User</span>
        </p>
        <Image
          src={UserPlaceholder}
          alt="user-placeholder"
          width={50}
          height={50}
          className="rounded-full"
        />
      </div>
    </div>
  );
}
