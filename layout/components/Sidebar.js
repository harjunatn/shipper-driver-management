import Link from "next/link";
import { useRouter } from "next/router";
import classNames from "classnames";

import { SidebarContainer } from "./Sidebar.styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { MENUS } from "../consts/menus";

export default function Sidebar({ showMenu }) {
  const router = useRouter();

  return (
    <SidebarContainer showMenu={showMenu}>
      {MENUS.map(({ link, icon, text }, index) => (
        <Link href={link} key={index}>
          <div className={classNames(router.pathname === link && "active")}>
            <FontAwesomeIcon icon={icon} className="mr-5 w-4" />
            {text}
          </div>
        </Link>
      ))}
    </SidebarContainer>
  );
}
