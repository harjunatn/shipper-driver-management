import styled from "styled-components";

export const SidebarContainer = styled.div`
  position: absolute;
  background: white;
  bottom: 0;
  top: 80px;
  width: 230px;
  z-index: 999;
  left: ${({ showMenu }) => (showMenu ? "0" : "-230px")};

  @media (min-width: 640px) {
    left: 0;
  }

  div {
    padding: 5px 10px;
    border-left: 3px solid transparent;
    cursor: pointer;

    :hover {
      color: #eb5546;
      border-left: 3px solid #eb5546;
    }
  }

  & .active {
    color: #eb5546;
    border-left: 3px solid #eb5546;
  }
`;
