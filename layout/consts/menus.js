import {
  faHome,
  faUser,
  faCalendarDays,
} from "@fortawesome/free-solid-svg-icons";

export const MENUS = [
  {
    link: "/",
    icon: faHome,
    text: "Beranda",
  },
  {
    link: "/driver",
    icon: faUser,
    text: "Driver Management",
  },
  {
    link: "/pickup",
    icon: faCalendarDays,
    text: "Pickup",
  },
];
