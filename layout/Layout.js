import { useState } from "react";

import Topbar from "./components/Topbar";
import Sidebar from "./components/Sidebar";

export default function Layout({ children }) {
  const [isShowMenu, setIsShowMenu] = useState(false);

  function onclickShowMenu() {
    setIsShowMenu(!isShowMenu);
  }

  return (
    <>
      <Topbar onclickShowMenu={onclickShowMenu} />
      <Sidebar showMenu={isShowMenu} />
      <div className="pl-10 sm:pl-64 pr-10 pt-8">{children}</div>
    </>
  );
}
