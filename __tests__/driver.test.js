import { render, screen } from "@testing-library/react";
import Driver from "../pages/driver";
import "@testing-library/jest-dom";

describe("Driver", () => {
  it("renders a page", () => {
    render(<Driver />);

    const filterContainer = screen.getByTestId("filter-container");
    expect(filterContainer).toBeInTheDocument();

    const filterInput = screen.getByTestId("filter-input");
    expect(filterInput).toBeInTheDocument();

    const filterButton = screen.getByTestId("filter-button");
    expect(filterButton).toBeInTheDocument();
  });
});
