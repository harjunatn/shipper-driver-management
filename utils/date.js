export function formatDate(date) {
  const format = date.split("T")[0];

  const split = format.split("-");
  return `${split[2]}-${split[1]}-${split[0]}`;
}
